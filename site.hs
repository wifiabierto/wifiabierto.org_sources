{-# LANGUAGE OverloadedStrings #-}
import Data.Map as M (lookup)
import Data.Maybe (fromMaybe)
import Data.Monoid (mappend)
import Hakyll
import Hakyll.Web.Pandoc
import System.Locale
import System.IO
import Text.Pandoc

staticPages = ["index.md", "comunidad.md", "CONTRIBUTING.md"]

featuredImageContext :: Context a
featuredImageContext = field "featuredImage" $ \item -> do
  metadata <- getMetadata (itemIdentifier item)
  return $ fromMaybe "" $ M.lookup "featured_image" metadata

myContext :: Context String
myContext = mappend defaultContext featuredImageContext

postContext :: Context String
postContext =
  dateFieldWith esTimeLocale "date" "%e de %B de %Y" `mappend`
  myContext

esTimeLocale :: TimeLocale
esTimeLocale = defaultTimeLocale {
  wDays  = [("Domingo", "Dom"), ("Lunes",     "Lun"),
            ("Martes",  "Mar"), ("Miércoles", "Mié"), 
            ("Jueves",  "Jue"), ("Viernes",   "Vie"), 
            ("Sábado",  "Sáb")],
  months = [("Enero",      "Ene"), ("Febrero",   "Feb"),
            ("Marzo",      "Mar"), ("Abril",     "Abr"),
            ("Mayo",       "May"), ("Junio",     "Jun"),
            ("Julio",      "Jul"), ("Agosto",    "Ago"),
            ("Septiembre", "Sep"), ("Octubre",   "Oct"),
            ("Noviembre",  "Nov"), ("Diciembre", "Dic")],
  amPm = ("a.m.", "p.m."),
  dateTimeFmt = "%a %b %e %H:%M:%S %Z %Y",
  dateFmt = "%d/%m/%y",
  time12Fmt = "%I:%M:%S %p"
  }

--- Scripts to take from bower to add to the concatenated main script
bowerConcatScripts = [ "bower_components/jquery/dist/jquery.min.js"
                     , "bower_components/fastclick/lib/fastclick.js"
---                     , "bower_components/jquery.cookie/jquery.cookie.js"
---                     , "bower_components/jquery-placeholder/jquery.placeholder.js"
                     , "bower_components/foundation/js/foundation.min.js"
                     , "bower_components/foundation/js/foundation/foundation.topbar.js"
                     ]

--- Pandoc custom setup
postWriterOptions :: WriterOptions
postWriterOptions = defaultHakyllWriterOptions { writerTableOfContents = True
                                               , writerTemplate = "<h2>Contenidos</h2>\n$toc$\n$body$"
                                               , writerStandalone = True
                                               }

main :: IO ()
main = hakyll $ do
  -- Blobs
  match "images/*" $ do
    route   idRoute
    compile copyFileCompiler

  -- Styles
  match "css/main.sass" $ do
    route   $ setExtension "css"
    compile $ getResourceString
      >>= withItemBody (unixFilter "sass" ["-s", "-I", "bower_components/foundation/scss"])
      >>= return . fmap compressCss

  -- Scripts
  create ["js/modernizr.js"] $ do
    route idRoute
    compile $ do
      makeItem ""
        >>= withItemBody (unixFilter "cat" ["bower_components/modernizr/modernizr.js"])

  create ["js/main.min.js"] $ do
    route idRoute
    compile $ do
      makeItem ""
        >>= withItemBody (unixFilter "./node_modules/.bin/uglifyjs" bowerConcatScripts)

  -- Static Pages
  match (fromList staticPages) $ do
    route   $ setExtension "html"
    compile $ pandocCompiler
      >>= loadAndApplyTemplate "templates/default.html" myContext
      >>= relativizeUrls

  -- Articles
  match "posts/*" $ do
    route $ setExtension "html"
    compile $ pandocCompilerWith defaultHakyllReaderOptions postWriterOptions
      >>= loadAndApplyTemplate "templates/post.html"    postContext
      >>= loadAndApplyTemplate "templates/default.html" postContext
      >>= relativizeUrls

  create ["archivo.html"] $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"
      let archiveCtx =
            listField "posts" postContext (return posts) `mappend`
            constField "title" "Artículos"               `mappend`
            defaultContext
      makeItem ""
        >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
        >>= loadAndApplyTemplate "templates/default.html" archiveCtx
        >>= relativizeUrls


  -- Templates
  match "templates/*" $ compile templateCompiler
