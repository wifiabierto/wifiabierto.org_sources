---
title: ¿Cómo configurar un Rasberry Pi como punto de acceso?
tags: raspberry-pi,routers,access-points,mesh,tutoriales
author: Sebastián Magrí
---

En este artículo se describe cómo configurar un [Raspberry Pi](http://raspberrypi.org/) como
punto de acceso para una red inalámbrica rápida.

El Raspberry Pi es un computador del tamaño de una tarjeta bancaria
utilizado para la implementación de prototipos de todo tipo en el
campo de la computación y la electrónica. Cuenta con un puerto
`Ethernet` y dos puertos `USB`, lo que lo hace perfecto para una
aplicación como `punto de acceso` inalámbrico.

En este tutorial, utilizaremos [Arch Linux](http://archlinux.org/)
como sistema operativo del Raspberry Pi. Esto nos da acceso a
actualizaciones recientes y una muy buena disponibilidad de
aplicaciones. Además, nos permite utilizar
[create_ap](https://github.com/oblique/create_ap), una utilidad para
la configuración rápida de puntos de acceso.

## Ingredientes

* Raspberry Pi
* Tarjeta SD de 2GiB como mínimo
* Adaptador Wireless USB
* Conexión a Internet
* Cable Ethernet

## Preparación

### 1. Obtener la imagen de Arch Linux para ARM

Descarga la última imagen de [Arch Linux para ARM](http://archlinuxarm.org/) desde la [página
específica para Raspberry
Pi](http://archlinuxarm.org/platforms/armv6/raspberry-pi).

[Enlace
directo](http://archlinuxarm.org/os/ArchLinuxARM-rpi-latest.zip)

Al descomprimir el archivo `ZIP` descargado, obtendrás un archivo
`IMG`, que es una imagen completa del sistema operativo para el
Raspberry Pi lista para cargar en la tarjeta SD.

### 2. Cargar la imagen de Arch Linux en la tarjeta SD

El procedimiento para *quemar* la imagen `IMG` en la tarjeta SD varía
de acuerdo al sistema operativo que se utilice, pero es bastante
sencillo en todos los casos.

#### 2.1 En sistemas tipo UNIX

Si estás usando un sistema operativo `UNIX`, como `Linux` o `Mac OS`,
debes identificar la tarjeta SD después de conectarla. Para esto,
**antes de conectar la tarjeta**, introduce el siguiente comando en un
terminal:

```sh
$ ls /dev/sd[a-z]
/dev/sda /dev/sdb
```

Luego puedes introducir la tarjeta SD y repetir el mismo comando:

```sh
$ ls /dev/sd[a-z]
/dev/sda /dev/sdb /dev/sdc
```

Como puedes observar en la salida de ejemplo, aparece un nuevo
elemento, `/dev/sdc`. Si no has introducido otro dispositivo en tu
computador, esa es la ruta de tu tarjeta SD.

Con la tarjeta identificada, podemos proceder a cargarle la imagen
`IMG`. Así, ubicado en el directorio donde está la imagen
descomprimida, puedes cargar la imagen con el siguiente comando, como
usuario `root`:

```sh
# ls *.img
ArchLinuxARM-2014.04-rpi.img
# dd if=ArchLinuxARM-2014.04-rpi.img of=/dev/sdc bs=1M
```

#### 2.2 En sistemas Windows

Si usas Windows, debes primero descargar
[Win32DiskImager](https://launchpad.net/win32-image-writer) e
instalarlo. Una vez instalado, ejecútalo y selecciona el archivo
descomprimido `IMG`, selecciona la letra de disco correspondiente a la
tarjeta SD y presiona `Write`.

### 3. Configurar el Raspberry Pi

Introduce la *tarjeta SD* con la imagen del sistema operativo cargada en
el Raspberry Pi y conecta el *cable Ethernet* desde el `router` que te
da acceso a internet hasta el puerto correspondiente. Enciéndelo
conectando su adaptador de corriente o un cable USB adecuado.

Al arrancar, el Pi debe obtener una dirección IP asignada de manera
dinámica por el `router`. Puedes averiguar la dirección IP asignada al
dispositivo en el panel de configuración de tu router. Por defecto el
nombre del Pi es `alarmpi`.

#### 3.1 Ingresar en el Pi

Para configurar el dispositivo es necesario ingresar en el mismo
usando `SSH`.

En sistemas tipo `UNIX`, lo más probable es que ya tengas `OpenSSH`
instalado. Puedes verificar en una línea de comandos:

```sh
$ type ssh
ssh is /usr/bin/ssh
```

Si la respuesta del comando es `ssh not found`, debes instalar OpenSSH
haciendo uso del sistema de manejo paquetes de tu sistema.

En Windows, puedes conectarte a un servidor `SSH` usando
[PuTTY](http://www.putty.org/).

El usuario y contraseña por defecto es `root` y `root`, respectivamente.

#### 3.2 Cambiar la contraseña de root

El primer cambio a realizar una vez en el Pi, es la contraseña del
usuario `root`. Este cambio es esencial debido a que el equipo va a
estar conectado a Internet y sirviendo de punto de acceso. Para esto,
ejecuta:

```sh
# passwd
```

#### 3.2 Actualizar e instalar el software necesario

Antes de instalar las aplicaciones necesarias, es buena idea
actualizar todo el Software instalado y reiniciar el equipo para
garantizar que esté corriendo con el núcleo más nuevo disponible.

```sh
# pacman -Syyu
# shutdown -r now
```

Al reiniciar y volver a entrar al equipo, después de la actualización,
es necesario instalar algunas herramientas básicas en el sistema que
sirven para obtener las utilidades para la configuración del punto de
acceso más adelante.

```sh
# pacman -S base-devel yaourt
# yaourt -Syy
```

Finalmente, se instala
[create_ap](https://github.com/oblique/create_ap), una utilidad que
facilita en gran medida la configuración rápida de un punto de acceso.

```sh
# yaourt -S create_ap
```

### 4. Activar el punto de acceso

Activar el punto de acceso es tan fácil como utilizar `create_ap` para
configurarlo e iniciarlo de manera automática. Esta utilidad ofrece
varias opciones al usuario, que se pueden visualizar ejecutando
`create_ap --help`.

Una descripción completa de la funcionalidad de `create_ap` está fuera
del alcance de este artículo, por lo que recomendamos leer el archivo
[README](https://github.com/oblique/create_ap/blob/master/README.md)
del proyecto si llegara a necesitar más detalles.

#### 4.1 Iniciar el punto de acceso en la línea de comandos

Para ésta aplicación, crearemos un punto de acceso con las siguientes
características:

* **Nombre del punto de acceso**: WifiAbierto_0
* **Contraseña**: WifiAbierto
* **Puerta de enlace**: 10.11.12.1

Además, si se quisiera compartir la conexión a Internet, es necesario
contar con una interfaz de red que ya esté conectada y saber su
identificador. `create_ap` permite compartir la conexión desde la
misma interfaz de red inalámbrica que sirve de punto de acceso. En
este caso, se utilizará la interfaz de red `eth0`, correspondiente por
defecto a la conexión Ethernet que hemos estado utilizando para
trabajar sobre el Raspberry Pi. Por otro lado, el identificador de la
interfaz de red inalámbrica es `wlan0`.

Con esto definido, se pasan las opciones a `create_ap` en la línea de
comandos de la siguiente manera:

```sh
# create_ap -g 10.11.12.1 wlan0 eth0 WifiAbierto_0 WifiAbierto
```

Esto arrancará el punto de acceso y quedará ejecutándose en el
terminal actual. Para *apagar* el punto de acceso, basta con ingresar
`Ctrl+c`.

#### 4.2 Iniciar el punto de acceso como servicio de fondo

Debido a que iniciar el punto de acceso en la línea de comandos
implicaría mantenerse conectado al Raspberry Pi, es posible activar el
punto de acceso como un servicio del sistema.

Al instalarse, `create_ap` provee un archivo de ejemplo para la
configuración de un servicio del sistema usando `systemd` que
se puede adaptar fácilmente a cada caso. La ruta del archivo es
`/usr/share/create_ap/create_ap.service.example`, y sus contenidos son

```config
[Unit]
Description=Create AP Service

[Service]
Type=simple
ExecStart=/usr/bin/bash create_ap -n -g 10.0.0.1 wlan0 AccessPointSSID
KillSignal=SIGINT
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
```

La línea de interés es la que comienza con `ExecStart`. Ahí es
necesario reemplazar la orden siguiente a `/usr/bin/bash` con la que
se utilizó previamente para iniciar el punto de acceso en la línea de
comandos. Para comenzar se crea una copia del archivo de ejemplo al
directorio de servicios del sistema:

```sh
# cp /usr/share/create_ap/create_ap.service.example /etc/systemd/system/punto_de_acceso.service
```

Posteriormente se modifica el archivo usando `nano`, un pequeño editor
de archivos.

```sh
# nano /etc/systemd/system/punto_de_acceso.service
```

Se actualiza la opción `ExecStart` para utilizar la orden de arranque
del punto de acceso. También se puede editar la descripción:

```config
[Unit]
Description=Iniciar punto de acceso WifiAbierto_0

[Service]
Type=simple
ExecStart=/usr/bin/bash create_ap -g 10.11.12.1 wlan0 eth0 WifiAbierto_0 WifiAbierto
KillSignal=SIGINT
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
```

Posiblemente sea necesario recargar el manejador de servicios para
tomar el nuevo archivo:

```sh
# systemctl daemon-reload
```

Para iniciar el servicio en el fondo, basta ahora con ejecutar:

```sh
# systemctl start punto_de_acceso
```

Se puede verificar el estado del servicio con la orden:

```sh
# systemctl status punto_de_acceso
```

Y *apagar* el servicio con:

```sh
# systemctl stop punto_de_acceso
```

Si se desea que el punto de acceso se inicie cada vez que arranque el
Raspberry Pi, se debe *activar* el servicio:

```sh
# systemctl enable punto_de_acceso
```

### 5. ¿Y si no hay un Raspberry Pi?

Este procedimiento se puede realizar practicamente sin modificaciones
en un computador, que bien puede ser uno abandonado de baja
gama. Una laptop vieja cuya tarjeta inalámbrica soporte el modo
promiscuo soporta sin problemas este procedimiento, igualmente.

Si deseas intentarlo, podemos ayudarte a través del [canal de
IRC](https://kiwiirc.com/client/irc.unplug.org.ve/wifiabierto) y de la
[lista de correos](mailto:wifiabierto@librelist.com).