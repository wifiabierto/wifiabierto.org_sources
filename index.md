---
title: Bienvenido al proyecto WifiAbierto.
featured_image: /images/wifiabierto-tagline.png
---

`WifiAbierto` es una iniciativa para hacer accesible a las comunidades el conocimiento y soporte necesarios para la instalación, configuración y mantenimiento de redes inalámbricas abiertas. La instalación de una `red inalámbrica abierta` pone al alcance de las comunidades herramientas para la autogestión, el acceso a información altamente local, y el desarrollo de proyectos para el mejoramiento de la calidad de vida de sus habitantes alrededor de las tecnologías de la información.

`WifiAbierto` nace del entendimiento de la **prioridad del acceso a Internet para el desarrollo de la sociedad**. Considerando, además, que el desenvolvimiento de las personas en la red debe contar con la **garantía de un conjunto de derechos irrenunciables** para el ejercicio libre y responsable de sus actividades. Igualmente, este proyecto nace de la consciencia sobre la capacidad de los ciudadanos de desarrollar proyectos que los potencien y los conecten en la búsqueda de la satisfacción de sus necesidades y la resolución de sus problemas.

Si compartes los ideales de `WifiAbierto` y deseas comenzar a trabajar en un `nodo` para tu comunidad, puedes revisar los [Artículos de Documentación](/archivo.html) y, por supuesto, [contactarnos y ayudarnos](/comunidad.html) a mejorar la base de conocimientos.
