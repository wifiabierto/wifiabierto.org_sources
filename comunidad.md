---
title: Únete a la Comunidad de WifiAbierto
---

## Contáctanos

Ponte en contacto con el resto de nosotros, aprende y mejora tu comunidad.

### Lista de Correos

Puedes suscribirte a nuestra lista enviando un correo a [wifiabierto@librelist.com](mailto:wifiabierto@librelist.com). No es necesario un asunto o contenido en particular.

Los archivos de la lista son públicos y se pueden leer en [librelist.com/browser/wifiabierto](http://librelist.com/browser/wifiabierto).

### Canal de conversación por IRC

El canal de IRC [#wifiabierto](https://kiwiirc.com/client/irc.unplug.org.ve/wifiabierto) en el servidor de charlas de [UNPLUG](http://www.unplug.org.ve/). Para conectarte con una aplicación cliente de IRC, puedes utilizar los siguientes datos:

* Servidor: irc.unplug.org.ve
* Puerto: 6697 (SSL)
* Canal: #wifiabierto

Si no tienes un cliente de IRC, puedes conectarte con tu navegador usando la [interfaz web del canal](https://kiwiirc.com/client/irc.unplug.org.ve/wifiabierto).

### Redes Sociales

* Twitter: @[WifiAbierto](https://twitter.com/wifiabierto)
* Facebook: [WifiAbierto](https://www.facebook.com/WifiAbierto)

## Ayúdanos

Todas las ideas y tareas del proyecto son gestionadas inicialmente en la [pizarra pública del proyecto](https://trello.com/b/xTzMevDl/wifiabierto) en [Trello](https://trello.com).

Si tienes una nueva idea o deseas ayudarnos con alguna, coméntalo a la [lista de correos](mailto:wifiabierto@librelist.com) para discutirlo y ayudarte a implementarla.

Si deseas colaborar con el sitio, puedes revisar la [guía para colaboradores del sitio](/CONTRIBUTING.html).
