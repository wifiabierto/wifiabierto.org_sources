---
title: ¿Cómo colaborar con el sitio Web del proyecto WifiAbierto?
---

Si deseas escribir artículos o implementar mejoras al sitio, este es el lugar con la información que necesitas.

El primer paso para colaborar con el sitio Web del proyecto es revisar la [pizarra pública del proyecto](https://trello.com/b/xTzMevDl/wifiabierto).

## Artículos para el sitio

Los artículos pendientes se mantienen en la lista `Documentación`. Puedes tomar uno de los artículos pendientes o proponer uno nuevo enviando un correo electrónico a la [lista de correos](mailto:wifiabierto@librelist.com) con todos los detalles. De ésta manera el artículo será añadido a la pizarra de trabajo y los miembros de la comunidad estarán informados para optimizar los esfuerzos y ofrecer apoyo.

No hay muchos requerimientos para un nuevo artículo, y la manera de enviarlo depende de las facilidades y habilidades de cada persona. Puedes enviar un nuevo artículo en el [formato abierto](https://www.fsf.org/es/documentos/opendocument) de tu preferencia o como un sencillo archivo de texto plano. Los mantenedores del sitio se encargarán de añadirlo al sitio tan pronto como puedan.

Si tienes conocimientos de [Git](http://git-scm.org/) y algún formato soportado de texto plano, puedes escribir tu artículo y añadirlo al directorio `posts/` del repositorio para luego enviarlo como un `Pull Request` al [proyecto en Bitbucket](https://bitbucket.org/wifiabierto/wifiabierto.org_sources). Es recomendable trabajar sobre cada artículo en una rama separada de `master`.

Más detalles sobre todos los formatos soportados para la documentación del sitio pueden verse en la página de [Pandoc, el conversor universal de textos](http://johnmacfarlane.net/pandoc/).

## Desarrollo

Si conoces sobre desarrollo Web y deseas mejorar el sitio, ¡bienvenido!.

El sitio es manejado con [Hakyll](http://jaspervdj.be/hakyll/), una aplicación para la generación de sitios estáticos. El código fuente se maneja en un repositorio público para el [proyecto en Bitbucket](https://bitbucket.org/wifiabierto/wifiabierto.org_sources). Ponte en contacto con nosotros a través de la [lista de correos](mailto:wifiabierto@librelist.com) o el canal de IRC [#wifiabierto](https://kiwiirc.com/client/irc.unplug.org.ve/wifiabierto) en [irc.unplug.org.ve](https://kiwiirc.com/client/irc.unplug.org.ve/) para ayudarte a tener tus cambios aplicados al sitio.
